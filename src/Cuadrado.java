/**
 * Created by Wilmer on 17/05/2017.
 */
public class Cuadrado implements Figura {
    private float lado;
    public Cuadrado (float lado) {
        this.lado = lado;
    }
    public float area() {
        return lado*lado;
    }
}
