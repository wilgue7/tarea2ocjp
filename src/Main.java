import EjemploHerenciaAgragacion.Institucion;

public class Main {

    public static void main(String[] args) {
        System.out.println("Hello World!");
        /**
         * usu interface
         */
        Figura cuad1 = new Cuadrado (3.5f);
        Figura cuad2 = new Cuadrado (2.2f);
        Figura cuad3 = new Cuadrado (8.9f);
        Figura circ1 = new Circulo (3.5f);
        Figura circ2 = new Circulo (4f);

        System.out.println(cuad1);
        System.out.println(cuad2);
        System.out.println(cuad3);
        System.out.println(circ1);
        System.out.println(circ2);
        /**
         * enums
         */
        Institucion  bancoGuayaquil=Institucion.BANCO_GUAYAQUIL ;


    }
}
