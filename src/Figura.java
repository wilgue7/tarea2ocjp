/**
 * Created by Wilmer on 17/05/2017.
 */
public interface Figura {
    float PI = 3.1416f;   // Por defecto public static  final. La f final indica que el número es float
    float area();   // Por defecto abstract public
}
