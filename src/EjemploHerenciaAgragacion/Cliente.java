package EjemploHerenciaAgragacion;

/**
 * Created by Wilmer on 17/05/2017.
 */
public class Cliente extends Cuenta {
    String nombre;
    int edad;

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getEdad() {
        return edad;
    }

    public void setEdad(int edad) {
        this.edad = edad;
    }

    public Cliente(String nombre) {
        this.nombre = nombre;
    }
}
