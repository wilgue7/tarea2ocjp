package EjemploHerenciaAgragacion;

/**
 * Created by Wilmer on 17/05/2017.
 */
public enum Institucion {


    BANCO_GUAYAQUIL("Ahorros",1),
    BANCO_PICHINCHA("ahorros",2),
    BANCO_PACIFICO("Corriente",3),
    BANCOINTERNACIONAL("Corriente",4);
    private String nombreCuenta;
    private int codigo;

    Institucion(String nombreCuenta, int codigo) {
        this.nombreCuenta = nombreCuenta;
        this.codigo = codigo;
    }

    public String getNombreCuenta() {
        return nombreCuenta;
    }

    public int getCodigo() {
        return codigo;
    }
}
